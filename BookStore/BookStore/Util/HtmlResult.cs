﻿using System;
using System.Web.Mvc;

namespace BookStore.Util
{
    public class HtmlResult : ActionResult
    {
        private String htmlCode;

        public HtmlResult(String html)
        {
            htmlCode = html;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            String fullHtmlCode = "<!DOCTYPE html><html><head>";
            fullHtmlCode += "<title>Головна сторінка</title>";
            fullHtmlCode += "<meta charset=utf-8 />";
            fullHtmlCode += "</head> <body>";
            fullHtmlCode += htmlCode;
            fullHtmlCode += "</body></html>";
            context.HttpContext.Response.Write(fullHtmlCode);
        }
    }

}