﻿using System.Data.Entity;

namespace BookStore.Models
{
    public class BookDbInitializer : DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext context)
        {
            context.Students.Add(new Student { Id = 1, Name = "Dummy Student", Prefomance = "A-" });
            context.Books.Add(new Book { Id = 1, Name = "Война и мир", Author = "Л. Толстой", Price = 220, Language = "Russian" });
            context.Books.Add(new Book { Id = 2, Name = "Отцы и дети", Author = "И. Тургенев", Price = 180, Language = "Russian" });
            context.Books.Add(new Book { Id = 3, Name = "Чайка", Author = "А. Чехов", Price = 150, Language = "English" });
            base.Seed(context);
        }
    }
}