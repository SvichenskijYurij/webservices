﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookStore.Models
{
    public class Book
    {
        // ID книги
        public Int32 Id { get; set; }
        // Назва книги
        [Display(Name = "Назва")]
        public String Name { get; set; }
        // Автор книги
        [Display(Name = "Автор")]
        public String Author { get; set; }
        // Ціна
        [Display(Name = "Ціна")]
        public Int32 Price { get; set; }
        [Display(Name = "Мова")]
        public string Language { get; set; }
        public int? StudentId { get; set; }
    }

}