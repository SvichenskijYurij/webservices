﻿using System;

namespace BookStore.Models
{
    public class Purchase
    {
        // ID покупки
        public Int32 PurchaseId { get; set; }
        // Ім'я та прізвище покупця
        public String Person { get; set; }
        // Адреса покупця
        public String Address { get; set; }
        // ID книги
        public Int32 BookId { get; set; }
        // Дата покупки
        public DateTime Date { get; set; }
    }

}