﻿using System.Collections.Generic;

namespace BookStore.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Prefomance { get; set; }
        public List<Book> Books { get; set; }
    }
}