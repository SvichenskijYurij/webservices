﻿using BookStore.Models;
using BookStore.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class HomeController : Controller
    {
        // Створюємо контекст даних
        BookContext db = new BookContext();

        public ActionResult Index(string bookName = null)
        {
            if(string.IsNullOrEmpty(bookName))
            {
                // Отримуємо з бд всі об'єкти Book
                IEnumerable<Book> books = db.Books;
                // Передаємо всі об'єкти в динамічну властивість Books у ViewBag
                ViewBag.Books = books;
                // Вертаємо представлення
            }
            else
            {
                var name = bookName.ToLower();
                ViewBag.Books = db.Books.Where(b => b.Name.ToLower().Contains(name)).ToList();
            }
            return View();
        }

        [HttpGet]
        public ActionResult About()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Students()
        {
            return View(db.Students);
        }

        [HttpGet]
        public ActionResult StudentProductivity(int? id)
        {
            return PartialView(db.Students.FirstOrDefault(s => s.Id == id));
        }

        [HttpGet]
        public ActionResult Author(int? id)
        {
            ViewBag.Author = db.Books.FirstOrDefault(b => b.Id == id).Author;
            return View();
        }

        [HttpGet]
        public ActionResult Buy(int? id)
        {
            ViewBag.BookId = id;
            return View();
        }

        [HttpPost]
        public string Buy(Purchase purchase)
        {
            purchase.Date = DateTime.Now;
            // Додаємо інформацію про покупку в базу даних
            db.Purchases.Add(purchase);
            // Зберігаємо в бд всі зміни
            db.SaveChanges();
            return "Спасибі," + purchase.Person + ", за покупку!";
        }

        public ActionResult GetImage()
        {
            String path = "../images/pepe.jpg";
            return new ImageResult(path);
        }

        public ActionResult GetHtml()
        {
            return new HtmlResult("<h2>Привіт світ!</h2>");
        }

        public ActionResult BookView(Int32? id)
        {
            var model = db.Books.Find(id);
            return View(model);
        }

        [HttpGet]
        public ActionResult EditBook(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Book book = db.Books.Find(id);
            if (book != null)
            {
                return View(book);
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult EditBook(Book book)
        {
            db.Entry(book).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Book book)
        {
            db.Books.Add(book);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Book b = db.Books.Find(id);
            if (b == null)
            {
                return HttpNotFound();
            }
            return View(b);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Book b = db.Books.Find(id);
            if (b == null)
            {
                return HttpNotFound();
            }
            db.Books.Remove(b);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}