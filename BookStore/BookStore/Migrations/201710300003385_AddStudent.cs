namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStudent : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Prefomance = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Books", "StudentId", c => c.Int(nullable: true));
            CreateIndex("dbo.Books", "StudentId");
            AddForeignKey("dbo.Books", "StudentId", "dbo.Students", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "StudentId", "dbo.Students");
            DropIndex("dbo.Books", new[] { "StudentId" });
            DropColumn("dbo.Books", "StudentId");
            DropTable("dbo.Students");
        }
    }
}
