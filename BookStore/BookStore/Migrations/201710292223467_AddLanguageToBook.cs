namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLanguageToBook : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "Language", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "Language");
        }
    }
}
