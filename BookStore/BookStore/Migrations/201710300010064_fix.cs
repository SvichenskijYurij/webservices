namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Books", "StudentId", "dbo.Students");
            DropIndex("dbo.Books", new[] { "StudentId" });
            AlterColumn("dbo.Books", "StudentId", c => c.Int());
            CreateIndex("dbo.Books", "StudentId");
            AddForeignKey("dbo.Books", "StudentId", "dbo.Students", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "StudentId", "dbo.Students");
            DropIndex("dbo.Books", new[] { "StudentId" });
            AlterColumn("dbo.Books", "StudentId", c => c.Int(nullable: false));
            CreateIndex("dbo.Books", "StudentId");
            AddForeignKey("dbo.Books", "StudentId", "dbo.Students", "Id", cascadeDelete: true);
        }
    }
}
